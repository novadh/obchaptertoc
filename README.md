# obchapstyles

# About

Provides `\chaptertoc` command. This package can be used with `oblivoir` class only.

See [http://www.ktug.org/xe/index.php?mid=KTUG_open_board&document_srl=244141] and/or 
[http://www.ktug.org/xe/index.php?document_srl=246352&mid=KTUG_QnA_board].



## oblivoir

The `obchaptertoc` package is included in [`kotex-oblivoir class`](https://github.com/kihwanglee/kotex-oblivoir) since 2020-03-03, oblivoir version 0.3.

